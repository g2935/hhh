#!/usr/bin/env bash


curl  -L -S --fail --max-time 320 --header 'Content-Type: application/json' --data '{ "Company_Name"            :   "Yeliz",
                                       "Company_AddressCountry"  :   "USA",
                                       "Company_AddressState"    :   "LA",
                                       "Company_AddressCity"     :   "sillicon",
                                       "Company_AddressStreet"   :   "Beautiful Street",
                                       "Company_AddressNumber"   :   "546",
                                       "Company_AddressPostCode" :   "10200",
                                       "Company_VATID"           :   "'$@'",
                                       "Company_BankAccName"     :   "MetaCorp",
                                       "Company_BankAccNumber"   :   "1225345345345"
                   }'  http://localhost:8000/create_company_account
