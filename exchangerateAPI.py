import sqlite3
from fastapi import FastAPI, Request
import uvicorn
app = FastAPI()


@app.post("/create_company_account")
async def create_company_account(payload: Request):
  values_dict = await payload.json()
  dbase = sqlite3.connect('database_group43.db', isolation_level=None)
  companies_with_this_vat=dbase.execute('''
    SELECT Company_VATID FROM Company
    WHERE Company_VATID=?
    ''',(str(values_dict['Company_VATID']),))
  companies = companies_with_this_vat.fetchall()
  if len(companies) != 0:
    raise Exception(f"companies_with_this_vat: {companies}")

  aaaa=dbase.execute('''INSERT INTO Company(
                      Company_Name,
                      Company_AddressCountry,
                      Company_AddressState,
                      Company_AddressCity,
                      Company_AddressStreet,
                      Company_AddressNumber,
                      Company_AddressPostCode,
                      Company_VATID,
                      Company_BankAccName,
                      Company_BankAccNumber) 
                      VALUES(?,?,?,?,?,?,?,?,?,?)''',(
                          str(values_dict["Company_Name"]), 
                          str(values_dict["Company_AddressCountry"]), 
                          str(values_dict["Company_AddressState"]),
                          str(values_dict["Company_AddressCity"]),
                          str(values_dict["Company_AddressStreet"]),
                          str(values_dict["Company_AddressNumber"]),
                          str(values_dict["Company_AddressPostCode"]),
                          str(values_dict["Company_VATID"]), 
                          str(values_dict["Company_BankAccName"]),
                          str(values_dict["Company_BankAccNumber"])))
  

  dbase.close()
  return True
  
 
 
if __name__ == '__main__':
  uvicorn.run(app, host='127.0.0.1', port=8000)

 